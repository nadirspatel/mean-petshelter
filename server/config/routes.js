var pets = require('../controllers/pets.js');
var path = require('path');
module.exports = function (app) {
    app.post('/api/pets', pets.create); // create pets on post
    app.get('/api/pets', pets.index); // get all pets on get
    app.get('/api/pets/detail/:id', pets.show); // show one pets on get
    app.put('/api/pets/edit/:id', pets.edit); // update one pets
    app.delete('/api/pets/:id', pets.destroy); // delete one pets on post
    app.all("*", (req, res, next) => {
        res.sendFile(path.resolve("./public/dist/public/index.html"));
    });
}