var mongoose = require('mongoose');
var Pet = mongoose.model('Pet');

module.exports = {
    //create Pet function for database
    create: function(req,res){
        console.log('hit create');
        var newPet = new Pet(req.body);
        newPet.save(function(err) {
            if(err){
                console.log('got errors');
                res.json(err);
            }else{
                console.log('success!');
                res.json(newPet);
            }
        })
    },
    //end create Pet function

    //list pets function for database
    index: function(req,res){
        console.log('hit index');
        Pet.find({}, function(err, pets){
            res.json(pets);
        })
    },
    // end list function


    //show Pet function for database
    show: function(req,res){
        console.log('hit index');
        Pet.findOne({_id: req.params.id}, function(err, Pet){
            res.json(Pet);
        })
    },
    // end show Pet function


    // // edit Pet function for database
    edit: function(req,res){
        console.log('hit edit Pet');
        Pet.findOne({_id: req.params.id}, function(err,Pet){
            if(err){
                console.log('got errors on edit');
                res.json(err);
            }else{
                console.log('edited success!');
                Pet.title = req.body.title;
                Pet.type = req.body.type;
                Pet.description = req.body.description;
                Pet.skill1 = req.body.skill1;
                Pet.skill2 = req.body.skill2;
                Pet.skill3 = req.body.skill3;
                Pet.likes = req.body.likes;
                Pet.save()
                res.json({message:"updated Pet"});
            }
        });
    },
    //end edit Pet function

    //begin delete Pet function for database
    destroy: function(req, res){
        console.log('hit delete Pet', req)
        Pet.remove({_id: req.params.id}, function(err){
            if(err){
                console.log("Error in destroy", err);
                res.json(err);
            } else {
                console.log("successful delete");
                res.json(true);
            }
        })
    },
    // end delete Pet function

    //begin like function
    like: function (req, res) {
        console.log("Hit like function");
        Pet.update({_id: req.params.id},{$inc: {likes: 1}}, (err) => {
            if(err){
                console.log("Error in like", err);
                return res.status(401).json(err);
            }
            return res.json("Congrats! You liked this pet!")
        })
    },
    // This finds the option whose id matches that in the route
    // parameter and then increments the like by 1

}