var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var PetSchema = new mongoose.Schema({
    title: {type: String, unique: true, required:true, minlength: 3},
    type: {type: String, required:true, minlength: 3},
    description: {type: String, required:true, minlength: 3},
    skill1: {type: String, required:true, minlength: 3},
    skill2: {type: String, required:true, minlength: 3},
    skill3: {type: String, required:true, minlength: 3},
    likes: {type: Number, default:0}
}, { timestamps: true });

PetSchema.plugin(uniqueValidator);

mongoose.model('Pet', PetSchema);