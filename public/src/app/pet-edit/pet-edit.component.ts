import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-pet-edit',
  templateUrl: './pet-edit.component.html',
  styleUrls: ['./pet-edit.component.css']
})
export class PetEditComponent implements OnInit {
  pet = {title: '', type: '', description: '', skill1: '', skill2: '', skill3: '', likes: ''};
  constructor(private _httpService: HttpService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
    console.log(params['id']);
    let tempObservable = this._httpService.getEditPetbyID(params['id']);
    tempObservable.subscribe((data: any) => {
      this.pet = data;
    });
    });
  }

  // begin edit pet
  editPet() {
    this._route.params.subscribe((params: Params) => {
    console.log('editPet');
    // use the service to make a post request to the backend express server
    let tempObservable = this._httpService.editPet(this.pet, params['id']);
    tempObservable.subscribe((data: any) => {
      console.log('got a response for edit pet', data);
      // any errors?
      if (!data.errors) {
        this._router.navigate(['/pets']);
      }
    });
  });
  }
  // end edit pet

  // cancel button
  cancel() {
    console.log('cancelling edit pet');
    this._router.navigate(['/pets']);
  }
  // end cancel button

}
