import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.css']
})
export class PetListComponent implements OnInit {
  pets = []; // now pets is defined for pet-list-component.html
  constructor(private _httpService: HttpService) { }

  ngOnInit() {
    // make a get request for all pets in database
    let tempObservable = this._httpService.getAllPets();
    tempObservable.subscribe((data: any) => {
      console.log('got a response reponse', data);
      this.pets = data;
    });
  }

  delete(objId){
    console.log('deleting record');
    let tempObservable = this._httpService.deletePet(objId);
    tempObservable.subscribe((data: any) => {
      console.log('record deleted successfully');
      if (!data.err) {
        this.ngOnInit();
      }
    });
  }

}
