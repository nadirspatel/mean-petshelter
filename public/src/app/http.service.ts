import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) { }
  postPet(petObj) {
    // actual post request happens here from form
    console.log('postPet, petObj is:', petObj);
    return this._http.post('/api/pets', petObj);
  }
  getAllPets() {
    console.log('getAllPets hit');
    return this._http.get('/api/pets');
  }
  getPetbyID(id) {
    console.log('getPetbyID hit');
    return this._http.get('/api/pets/' + id);
  }
  getEditPetbyID(id) {
    console.log('getEditPetbyID hit');
    return this._http.get('/api/pets/detail/' + id);
  }
  editPet(petObj, objId) {
    console.log('updatePetbyID hit', petObj, objId);
    return this._http.put('/api/pets/edit/' + objId, petObj);
  }
  getDetailPetbyID(id) {
    console.log('getDetailPetbyID hit');
    return this._http.get('/api/pets/detail/' + id);
  }
  deletePet(objId) {
    console.log('deletePet hit', objId);
    return this._http.delete('/api/pets/' + objId);
  }
  likePet(id) {
    console.log('likePet hit', id);
    return this.likePet('/api/pets/' + id);
  }

}
