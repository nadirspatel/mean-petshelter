import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pet-create',
  templateUrl: './pet-create.component.html',
  styleUrls: ['./pet-create.component.css']
})
export class PetCreateComponent implements OnInit {
  newPet = {title: '', type: '', description: '', skill1: '', skill2: '', skill3: '', likes: ''};
  constructor(private _httpService: HttpService, private _router: Router) { }

  ngOnInit() {
  }
  //create pet on post
  createPet() {
    console.log('createPet');
    // use the service to make a post request to the backend express server
    let tempObservable = this._httpService.postPet(this.newPet);
    tempObservable.subscribe((data: any) => {
      console.log('got a response', data);
      this.newPet = {title: '', type: '', description: '', skill1: '', skill2: '', skill3: '', likes: ''};
      // any errors?
      if (!data.errors) {
        this._router.navigate(['/pets']);
      }
    });
  }
  //end create pet on post

  // cancel button
  cancel() {
    console.log('cancelling new pet');
    this._router.navigate(['/pets']);
  }
  // end cancel button

}
