import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styleUrls: ['./pet-detail.component.css']
})
export class PetDetailComponent implements OnInit {
  pet = {title: '', type: '', description: '', skill1: '', skill2: '', skill3: '', likes: ''};
  constructor(private _httpService: HttpService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
    console.log(params['id']);
    let tempObservable = this._httpService.getDetailPetbyID(params['id']);
    tempObservable.subscribe((data: any) => {
      this.pet = data;
    });
    });
  }

  //begin delete pet from detail page
  delete(objId){
    console.log('adopting pet and deleting record...');
    let tempObservable = this._httpService.deletePet(objId);
    tempObservable.subscribe((data: any) => {
      console.log('record deleted successfully');
      this._router.navigate(['/pets']);
      if (!data.err) {
        this.ngOnInit();
      }
    });
  }
  // end delete pet from detail page



}
